# Pgh Parcels And Addresses App

```
cp variables.env.example variables.env
docker-compose up
```
Paste your mapbox token into the variables.env file

Open http://localhost:3001 in your browser

To run yarn/npm commands, while the container is running:

```
docker-compose exec app bash
```

That will give you a bash terminal inside the container in the app directory.
